﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace IzyckiP.Core
{
    public static class EnumerableExt
    {
        /// <summary>
        /// Generates random index and retrieves item within given collection.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="enumerable">Collection.</param>
        /// <returns>Randomly picked item.</returns>
        public static T PickRandom<T>(this IEnumerable<T> enumerable)
        {
            if (enumerable.Any() == false)
                throw new ArgumentException("Collection is empty.");

            return new List<T>(enumerable)[new Random().Next(enumerable.Count() - 1)];
        }

        /// <summary>
        /// Shuffles collection and retrieves specified number of items.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="enumerable">Collection.</param>
        /// <param name="count">Number of items to return.</param>
        /// <returns>Randomly picked items.</returns>
        public static IEnumerable<T> PickRandom<T>(this IEnumerable<T> enumerable, int count)
        {
            if (count > enumerable.Count())
                throw new ArgumentException("Ordered items numbers is greater than collection size.");

            return enumerable.Shuffle().Take(count);
        }

        /// <summary>
        /// Shuffles collection of items.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="enumerable">Collection.</param>
        /// <returns>Shuffled collection.</returns>
        public static IEnumerable<T> Shuffle<T>(this IEnumerable<T> enumerable)
        {
            var list = enumerable.ToList();
            var random = new Random();
            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = random.Next(n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
            return list;
        }

        /// <summary>
        /// Joins two collections into dictionary.
        /// </summary>
        /// <typeparam name="TKey"Key type.></typeparam>
        /// <typeparam name="TValue">Value type</typeparam>
        /// <param name="keys">Collection of keys.</param>
        /// <param name="values">Collection of values.</param>
        /// <returns>Dictionary.</returns>
        public static IDictionary<TKey, TValue> ZipToDictionary<TKey, TValue>(IEnumerable<TKey> keys, IEnumerable<TValue> values)
        {
            if (keys.Count() != values.Count())
                throw new CollectionsLengthNotEqualException();

            return keys.Zip(values, (k, v) => new { Key = k, Value = v }).ToDictionary(x => x.Key, x => x.Value);
        }
    }

    public class CollectionsLengthNotEqualException : Exception
    {
        public CollectionsLengthNotEqualException()
            : base()
        {
        }
    }
}
