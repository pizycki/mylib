﻿using System.Configuration;

namespace IzyckiP.Core
{
    public class ConfigHelper
    {
        public static string GetValueFor(string configKey)
        {
            return ConfigurationManager.AppSettings[configKey];
        }
    }
}
