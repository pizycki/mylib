﻿namespace IzyckiP.Core
{
    public static class IntegerExt
    {
        public static bool IsPositive(this int @int)
        {
            return @int > 0;
        }
    }

    public static class NullableIntegerExt
    {
        public static bool IsPositive(this int? @int)
        {
            return @int != null ? @int.Value.IsPositive() : false;
        }
    }
}
