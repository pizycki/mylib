﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace IzyckiP.Core
{
    /// <summary>
    /// Static wrapper.
    /// </summary>
    public static class StaticMapper
    {
        public static void Map<TSource, TTarget>(TSource source, TTarget target)
        {
            var mapper = new Mapper<TSource, TTarget>();
            mapper.Map(source, target);
        }

        public static TTarget CreateAndMap<TSource, TTarget>(TSource source)
            where TTarget : new()
        {
            var mapper = new Mapper<TSource, TTarget>();
            return mapper.CreateAndMap(source);
        }
    }

    public class Mapper<TSource, TTarget>
    {
        protected readonly List<Action<TSource, TTarget>> _mappings = new List<Action<TSource, TTarget>>();

        public IReadOnlyList<Action<TSource, TTarget>> Mappings
        {
            get { return _mappings; }
        }

        protected virtual void CopyMatchingProperties(TSource source, TTarget target)
        {
            var propertiesToRewrite = typeof(TTarget)
                .GetProperties(BindingFlags.Public | BindingFlags.Instance)
                .Where(p => p.CanWrite);

            foreach (var targetProp in propertiesToRewrite)
            {
                var sourceProp = typeof(TSource)
                    .GetProperties(BindingFlags.Public | BindingFlags.Instance)
                    .Where(p =>
                        p.Name == targetProp.Name
                        && p.PropertyType == targetProp.PropertyType
                        && p.CanRead)
                    .FirstOrDefault();

                if (sourceProp != null)
                    targetProp.SetValue(target, sourceProp.GetValue(source, null), null);
            }
        }

        protected virtual void ExecuteCustomMappings(TSource source, TTarget target)
        {
            foreach (var action in _mappings)
                action(source, target);
        }

        public virtual Mapper<TSource, TTarget> AddMap(Action<TSource, TTarget> mapping)
        {
            _mappings.Add(mapping);
            return this;
        }

        public virtual TTarget Map(TSource source, TTarget target)
        {
            CopyMatchingProperties(source, target);
            ExecuteCustomMappings(source, target);
            return target;
        }

        public virtual TTarget CreateAndMap(TSource source)
        {
            var type = typeof(TTarget);
            if (type.IsClass == false || (type.IsClass && type.IsAbstract))
                throw new UninitializableTargetTypeException();

            TTarget dest = Activator.CreateInstance<TTarget>();
            return Map(source, dest);
        }
    }

    #region [ Mapper exceptions ]

    public class MapperException : Exception
    {
        public MapperException(string msg)
            : base(msg)
        {
        }
    }

    public class UninitializableTargetTypeException : MapperException
    {
        public UninitializableTargetTypeException()
            : base("Targeted type is not a class or any other initializable type.")
        {
        }
    }

    #endregion
}
