﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace IzyckiP.Core
{
    /// <summary>
    /// Provides helping methods for paging. This is often used in pages with tables.
    /// </summary>
    public static class Paging
    {
        /// <summary>
        /// Retrieves elements from sorted and paged collection.
        /// </summary>
        public static IQueryable<T> Page<T, TOrderKey>(this IQueryable<T> collection, Expression<Func<T, TOrderKey>> orderSelector, int pageNumber, int pageSize)
        {
            return collection.OrderBy(orderSelector).Page<T>(pageNumber, pageSize);
        }

        /// <summary>
        /// Retrieves elements from paged collection.
        /// </summary>
        public static IQueryable<T> Page<T>(this IQueryable<T> collection, int pageNumber, int pageSize)
        {
            return collection.Skip((pageNumber - 1) * pageSize).Take(pageSize);
        }

        /// <summary>
        /// Calculates number of pages for pager to render. 
        /// </summary>
        /// <param name="itemsNumber">Items shown on single page.</param>
        /// <param name="pageSize">Item capacity of the table in the single page.</param>
        /// <returns>Number of total pages.</returns>
        public static int TotalPages(int itemsNumber, int pageSize)
        {
            return pageSize.IsPositive() ? (int)Math.Ceiling((double)itemsNumber / pageSize) : 0;
        }

        /// <summary>
        /// Calculates page number of selected index.
        /// </summary>
        /// <param name="index">Index of element in collection</param>
        /// <param name="pageSize">Item capacity of the table in the single page.</param>
        /// <returns>Page number of given element.</returns>
        public static int PageNumber(int index, int pageSize)
        {
            return pageSize.IsPositive() ? (int)Math.Ceiling((double)index / pageSize) : 0;
        }
    }
}
