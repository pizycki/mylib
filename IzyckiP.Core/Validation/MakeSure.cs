﻿using System;
using System.CodeDom;

namespace IzyckiP.Core.Validation
{
    public static class MakeSure
    {
        public static void That(bool condition, string comment = "")
        {
            if (condition == false)
                throw new ConditionNotMetException(comment);
        }

        public static void NotNull(object instance, string comment = "")
        {
            if (instance == null)
                throw new ObjectIsNullException(comment);
        }
    }

    public class ObjectIsNullException : Exception
    {
        public ObjectIsNullException(string comment) : base(comment)
        {
        }
    }


    public class ConditionNotMetException : Exception
    {
        public ConditionNotMetException(string text) : base(text)
        {
        }
    }
}
