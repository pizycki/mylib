﻿using System.Collections.Generic;
using System.Linq;

namespace IzyckiP.Core
{
    public static class StringExt
    {
        /// <summary>
        /// Splits text by looking for given separator.
        /// </summary>
        /// <param name="text">Text to be splited.</param>
        /// <param name="separator">Separator.</param>
        /// <param name="noDuplicates">Omits duplicates.</param>
        /// <param name="caseSensitive">Should cares about letters case.</param>
        /// <returns>Collection of substrings.</returns>
        public static IEnumerable<string> Split(this string text, string separator = ", ", bool noDuplicates = false, bool caseSensitive = true)
        {
            if (string.IsNullOrWhiteSpace(text))
                return new List<string>();

            var splited = text.Split(separator.ToCharArray())
                .Where(x => !string.IsNullOrEmpty(x));

            if (noDuplicates)
                splited = caseSensitive
                    ? splited.Distinct()
                    : splited.Distinct(new CaseInsensitiveStringComparer());

            return splited;
        }

        /// <summary>
        /// Joins all strings into one string seperated with separator.
        /// </summary>
        public static string GetAsSingleString(this IEnumerable<string> collection, string separator)
        {
            var result = string.Empty;
            collection = collection.ToList();

            if (!collection.Any())
                return result;

            result = collection.Count() == 1 ? collection.First() : string.Join(separator, collection);
            return result;
        }
    }

    /// <summary>
    /// Compares two string ignoring their case.
    /// </summary>
    class CaseInsensitiveStringComparer : IEqualityComparer<string>
    {
        public bool Equals(string x, string y)
        {
            return x.ToUpper() == y.ToUpper();
        }

        public int GetHashCode(string obj)
        {
            return obj.ToUpper().GetHashCode();
        }
    }
}
