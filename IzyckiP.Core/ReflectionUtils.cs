﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace IzyckiP.Core
{
    public class ReflectionUtils
    {
        /// <summary>
        /// Gets properties name and all members leading to this property.
        /// </summary>
        /// <typeparam name="T">Class containing property.</typeparam>
        /// <param name="propertyLambda"></param>
        /// <returns>Dot splited members and property name.</returns>
        /// <example>object.member.PropertyName</example>
        public static string GetAllMembersAndPropertyName<T>(Expression<Func<T>> propertyLambda)
        {
            var memberExpr = propertyLambda.Body as MemberExpression;

            if (memberExpr == null)
                throw new ArgumentException("You must pass a lambda of the form: '() => Class.Property' or '() => object.Property'");

            var result = string.Empty;
            do
            {
                result = memberExpr.Member.Name + "." + result;
                memberExpr = memberExpr.Expression as MemberExpression;
            } while (memberExpr != null);

            result = result.Remove(result.Length - 1); // remove the trailing "."
            return result;
        }

        /// <summary>
        /// Gets properties name.
        /// </summary>
        /// <typeparam name="T">Class containing property.</typeparam>
        /// <param name="propertyLambda"></param>
        /// <returns>Properties name.
        public static string GetPropertyName<T>(Expression<Func<T>> propertyLambda)
        {
            var fullPropertyName = GetAllMembersAndPropertyName<T>(propertyLambda);
            var elements = fullPropertyName.Split('.');
            var numberOfElements = elements.Length;
            return elements[numberOfElements - 1]; // Return last one.
        }

        /// <summary>
        /// By reflection retrieves value of property.
        /// </summary>
        public static T GetPropertyValue<T>(object obj, string propertyName)
        {
            object result = GetPropertyValue(obj, propertyName);
            return result == null ? default(T) : (T)result;
        }

        /// <summary>
        /// By reflection retrieves infos about all public and instanceable properties.
        /// </summary>
        public static IEnumerable<PropertyInfo> GetPropertyInfos(Type type)
        {
            return type.GetProperties(BindingFlags.Public | BindingFlags.Instance).Where(p => p.CanRead && p.CanWrite);
        }

        /// <summary>
        /// By reflection retrieves value of property.
        /// </summary>
        public static object GetPropertyValue(object obj, string propertyName)
        {
            var type = obj.GetType();
            var propInfo = type.GetProperty(propertyName);
            return propInfo.GetValue(obj, null);
        }
    }
}