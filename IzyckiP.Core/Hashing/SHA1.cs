﻿using System.Security.Cryptography;
using System.Text;

namespace IzyckiP.Core.Hashing
{
    public static class SHA1
    {
        public static byte[] GetHashToBytes(string inputString)
        {
            HashAlgorithm algorithm = System.Security.Cryptography.SHA1.Create();
            return algorithm.ComputeHash(Encoding.UTF8.GetBytes(inputString));
        }

        public static string HashToString(string inputString)
        {
            StringBuilder sb = new StringBuilder();
            foreach (byte b in GetHashToBytes(inputString))
                sb.Append(b.ToString("X2"));

            return sb.ToString();
        }
    }
}
