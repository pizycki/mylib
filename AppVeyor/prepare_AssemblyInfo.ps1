﻿################################################
#   Author: Paweł Iżycki
#   This script finds (recursively) all AssemblyInfo.cs files and update them
#   with actual version.
#   Requires build version to be passed in as enviroment variable.
#   This script was written for AppVeyor (http://www.appveyor.com) environment.
################################################

$version = $env:APPVEYOR_BUILD_VERSION
$asmbInfos = Get-ChildItem -Recurse | where { $_.Name -eq "AssemblyInfo.cs" }

foreach ($item in $asmbInfos)
{
    write ("Processing {0}" -f $item.FullName)
    $asmbInfoContent = Get-Content -Encoding UTF8 $item.FullName
    $asmbInfoContent = $asmbInfoContent -replace '1.0.0.0', $version
    $asmbInfoContent | Out-File $item.FullName
}