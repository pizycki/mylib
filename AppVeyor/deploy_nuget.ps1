﻿################################################
#   Author: Paweł Iżycki
#   This script prepares Nuget (http://nuget.org) packages and deploy them to
#   official Nuget gallery. It shall be placed in the root of solution folder
#   with project folders as children.
#   Requires Nuget API key to be passed in as enviroment variable.
#   This script was written for AppVeyor (http://www.appveyor.com) environment.
################################################

if( $env:APPVEYOR -eq $false) 
{
    write "Remove all old package folders"
    gci -Recurse | where { $_.BaseName -eq "package" } | Remove-Item -Recurse
}

write "Prepare nuget specs..."

$specs = gci -Recurse | where { $_.Extension -eq ".nuspec_template" }
$specCounter = 0
foreach ($spec in $specs)
{
    write ("Processing spec {0}/{1}" -f ++$specCounter, $specs.Length)

    write ("Spec name is {0}" -f $spec.BaseName)
    write ("Parent is {0}" -f $spec.DirectoryName)

    write "Get all DLL libraries with the same BaseName within the same folder spec was found (recursively)."
    $dlls = gci -Recurse -Path $spec.DirectoryName `
            | where { $_.Extension -eq ".dll" }
    
    $assemblies = $dlls | where { $_.BaseName -eq $spec.BaseName }

    if ($assemblies.Length -gt 0)
    {
        write "Rewrite version to Nuspec"
        $version = $env:APPVEYOR_BUILD_VERSION
        if ( [string]::IsNullOrWhiteSpace($version) ) { throw "Build version is not set" }

        write ("Build version: {0}" -f $version)
        $specContent = (Get-Content -Encoding UTF8 $spec.FullName)
        $specContent = $specContent -replace '\$version\$', $version
        
        write "Prepare directory for package content"
        $packageDir = $spec.DirectoryName + "/package"
        if ( !(Test-Path $packageDir) ) { mkdir $packageDir | Out-Null  }
        
        write "Create nuget spec basing on template"
        $newSpecPath = $packageDir + "/" + ($spec.Name -replace ".nuspec_template", ".nuspec")
        if (Test-Path $newSpecPath){ Remove-Item $newSpecPath }
        $specContent | Out-File $newSpecPath

        write "Create directory for package libraries"
        $libDir = ("{0}\lib\net45" -f $packageDir)
        if ( !(Test-Path $libDir) ) { mkdir $libDir | Out-Null }
        
        write "Creating nuget package."
        nuget pack $newSpecPath -OutputDirectory $packageDir -Verbosity "detailed"
        
        # Get Nuget APIKey from deploy env. variables.
        $apikey = $env:NUGET_APIKEY
        if ( [string]::IsNullOrWhiteSpace($apikey) -eq $true )
        {
            write "Nuget APIKey was not provided. Check your Env. variables."
        }
        else
        {
            write "Get all packages and push them."
            $packages = gci -Path $packageDir | where { $_.Extension -eq ".nupkg" }
            if ($packages.Length -gt 0)
            {
                $pkg = $packages[0]
                write "Pushin to nuget.org"
                nuget push $pkg.FullName $apikey -source "https://nuget.org/"
            }
        }
    }
}