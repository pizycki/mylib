﻿using System.Linq;
using System.Web;

namespace IzyckiP.Core.Mvc
{
    public static class HttpPostedFileBaseExt
    {
        /// <summary>
        /// Check is the file not null and its content is greater then zero.
        /// </summary>
        /// <param name="file">Posted file.</param>
        public static bool HasFile(this HttpPostedFileBase file)
        {
            return file != null && file.ContentLength > 0;
        }

        /// <summary>
        /// Splits file name by dot mark and returns last member.
        /// </summary>
        /// <param name="file">Posted file.</param>
        public static string FileExtension(this HttpPostedFileBase file)
        {
            if (file == null) return null;

            if (!file.FileName.Contains('.')) return string.Empty;

            var splited = file.FileName.Split('.');
            return splited.Last();
        }

        /// <summary>
        /// Splits file name by dot mark and returns last member adding dot in the begining.
        /// </summary>
        /// <param name="file">Posted file.</param>
        public static string FileExtensionAndDot(this HttpPostedFileBase file)
        {
            return "." + FileExtension(file);
        }
    }
}
