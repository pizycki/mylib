﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Resources;

namespace IzyckiP.Core.Mvc
{
    /// <summary>
    /// Sepcifies field name.
    /// </summary>
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property, AllowMultiple = false)]
    public class LocalizedNameAttribute : DisplayNameAttribute
    {
        private readonly string _resourceKey;
        private readonly ResourceManager _resource;
        public LocalizedNameAttribute(string resourceKey, Type resourceType)
        {
            _resource = new ResourceManager(resourceType);
            _resourceKey = resourceKey;
        }

        public override string DisplayName
        {
            get
            {
                string displayName = _resource.GetString(_resourceKey);

                return string.IsNullOrEmpty(displayName)
                    ? string.Format("[[{0}]]", _resourceKey)
                    : displayName;
            }
        }
    }

    public static class LocalizedNameHelper
    {
        public static string GetName<T>(T @object)
        {
            if (@object == null) return null;

            string locName = @object.ToString();
            var fieldInfo = @object.GetType().GetField(locName);
            var attributes =
                (DisplayNameAttribute[])fieldInfo.GetCustomAttributes(typeof(LocalizedNameAttribute), false);

            return attributes.Any() ? attributes[0].DisplayName : locName;
        }
    }
}
