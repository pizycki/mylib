﻿using System;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace IzyckiP.Core.Mvc.HtmlHelpers
{
    public static class LabelHtmlHelper
    {
        /// <summary>
        /// Gets formated label for model property from Resources. All HTML tags remain.
        /// </summary>
        /// <example>@Html.LabelForWithHtml(model => model.LabeledProperty)</example>
        public static HtmlString LabelForWithHtml<TModel, TValue>(this HtmlHelper<TModel> html,
                                                                    Expression<Func<TModel, TValue>> expression)
        {
            var label = LabelExtensions.LabelFor(html, expression);
            var decoded = HttpUtility.HtmlDecode(label.ToHtmlString());
            return new HtmlString(decoded);
        }
    }
}
