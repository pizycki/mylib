﻿using System;

namespace IzyckiP.Core.Mvc
{
    public static partial class EnumExt
    {
        public static string GetLocalizedName(this Enum @enum)
        {
            return @enum != null ? LocalizedNameHelper.GetName(@enum) : null;
        }
    }
}
