﻿using System.Collections.Generic;
using System.Linq;
using IzyckiP.Core.Validation;
using NUnit.Framework;

namespace IzyckiP.Core.Tests.Validation
{
    [TestFixture]
    public class MakeSureTests
    {
        [Test]
        public void MakeSure_a_equals_b_AndPass()
        {
            const short a = 2;
            const short b = 2;
            MakeSure.That(a == b);
        }

        [Test]
        public void MakeSure_DoesntContainsEvenNumbers_AndPass()
        {
            var numbers = new List<int> { 1, 3, 5 };
            MakeSure.That(numbers.All(x => x % 2 != 0));
        }

        [Test]
        [ExpectedException(typeof(ConditionNotMetException))]
        public void MakeSure_DoesntContainsEvenNumbers_AndFail()
        {
            var numbers = new List<int> { 1, 2, 3 };
            MakeSure.That(numbers.All(x => x % 2 != 0));
        }

        [Test]
        public void MakeSure_ObjectIsNotNull_AndPass()
        {
            object objectToTest = new object();
            MakeSure.NotNull(objectToTest);
        }

        [Test]
        [ExpectedException(typeof(ObjectIsNullException))]
        public void MakeSure_ObjectIsNotNull_AndFail()
        {
            object objectToTest = null;
            // ReSharper disable once ExpressionIsAlwaysNull
            MakeSure.NotNull(objectToTest);
        }
    }
}
