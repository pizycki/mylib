﻿using NUnit.Framework;

namespace IzyckiP.Core.Tests.StringsTests
{
    [TestFixture]
    public class SplitTests
    {
        public readonly string TextToBeSplited = "Ala, ala, has, cat,  , Ala";

        [Test]
        public void Should_split_basic_text()
        {
            var expected = new string[] { "Ala", "ala", "has", "cat", "Ala" };

            var splited = TextToBeSplited.Split(separator: ", ");

            Assert.AreEqual(expected, splited);
        }

        [Test]
        public void Should_split_basic_text_without_duplicates()
        {
            var expected = new string[] { "Ala", "ala", "has", "cat" };

            var splited = TextToBeSplited.Split(separator: ", ", noDuplicates: true);

            Assert.AreEqual(expected, splited);
        }

        [Test]
        public void Should_split_basic_text_without_duplicates_and_not_case_sensitve()
        {            
            var expected = new string[] { "Ala", "has", "cat" };

            var splited = TextToBeSplited.Split(separator: ", ", noDuplicates: true, caseSensitive: false);

            Assert.AreEqual(expected, splited);
        }
    }
}
