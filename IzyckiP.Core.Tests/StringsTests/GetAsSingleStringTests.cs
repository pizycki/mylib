﻿using NUnit.Framework;

namespace IzyckiP.Core.Tests.StringsTests
{
    [TestFixture]
    public class GetAsSingleStringTests
    {
        const string Separator = ", ";

        [Test]
        public void Should_join_collection_of_strings()
        {
            // Arrange
            var collectionOfStrings = new[] { "Ala", "ma", "kota" };
            var expected = "Ala, ma, kota";

            // Act
            var singleString = collectionOfStrings.GetAsSingleString(Separator);

            // Assert
            Assert.AreEqual(expected, singleString);
        }
    }
}
