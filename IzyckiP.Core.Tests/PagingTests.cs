﻿using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

namespace IzyckiP.Core.Tests
{
    [TestFixture]
    public class PagingTests
    {
        /// <summary>
        /// Page item capacity.
        /// </summary>
        public const int PageSize = 20;

        [TestCase(-3, Result = 0)]
        [TestCase(0, Result = 0)]
        [TestCase(10, Result = 1)]
        [TestCase(PageSize, Result = 1)]
        [TestCase(PageSize + 10, Result = 2)]
        [TestCase(2 * PageSize + 1, Result = 3)]
        public int Should_calculate_number_of_total_pages(int itemsNumber)
        {
            return Paging.TotalPages(itemsNumber, PageSize);
        }

        [TestCase(-1, Result = 0)]
        [TestCase(0, Result = 0)]
        [TestCase(1, Result = 1)]
        [TestCase(PageSize + 1, Result = 2)]
        public int Should_calculate_number_of_actual_page(int index)
        {
            return Paging.PageNumber(index, PageSize);
        }


    }

    [TestFixture]
    public class PagingCollections
    {
        [Test]
        public void Should_page_queryable_collection()
        {
            // Arrange 
            var collection = new List<Dummy>()
            {
                new Dummy { Id = 7 },
                new Dummy { Id = 3 },
                new Dummy { Id = 5 },
            };

            // Act
            var pagedCollection = collection.AsQueryable().Page(1, 1);

            // Assert
            Assert.AreEqual(7, pagedCollection.First().Id);
            Assert.AreEqual(1, pagedCollection.Count());
        }

        [Test]
        public void Should_page_ordered_queryable_collection()
        {
            // Arrange 
            var collection = new List<Dummy>()
            {
                new Dummy { Id = 7 },
                new Dummy { Id = 3 },
                new Dummy { Id = 5 },
            };

            // Act
            var pagedCollection = collection.AsQueryable().Page(x => x.Id, 1, 1);

            // Assert
            Assert.AreEqual(3, pagedCollection.First().Id);
            Assert.AreEqual(1, pagedCollection.Count());
        }

        class Dummy
        {
            public int Id { get; set; }
        }
    }
}
