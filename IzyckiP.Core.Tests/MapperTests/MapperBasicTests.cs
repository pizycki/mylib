﻿using System;
using IzyckiP.Core.Tests.Dummies;
using NUnit.Framework;

namespace IzyckiP.Core.Tests.MapperTests
{
    [TestFixture]
    public class MapperBasicTests
    {
        [Test]
        public void Should_map_objects_of_same_class()
        {
            long birthDateInTicks = DateTime.MinValue.Ticks;
            var contactNumber = "2939349575";

            // Arrange
            var source = new Person
            {
                BirthDate = new DateTime(birthDateInTicks),
                ContactNumber = contactNumber
            };

            var target = new Person();

            // Act
            var mapper = new Mapper<Person, Person>();
            mapper.Map(source, target);

            // Assert
            Assert.IsFalse(object.ReferenceEquals(source, target));
            Assert.AreEqual(birthDateInTicks, target.BirthDate.Ticks);
            Assert.AreEqual(contactNumber, target.ContactNumber);

        }

        [Test]
        public void Should_create_and_map_object_of_same_class()
        {
            long birthDateInTicks = DateTime.MinValue.Ticks;
            var contactNumber = "2939349575";

            // Arrange
            var source = new Person
            {
                BirthDate = new DateTime(birthDateInTicks),
                ContactNumber = contactNumber
            };

            // Act
            var mapper = new Mapper<Person, Person>();
            var target = mapper.CreateAndMap(source);

            // Assert
            Assert.IsNotNull(target);
            Assert.IsFalse(object.ReferenceEquals(source, target));
            Assert.AreEqual(birthDateInTicks, target.BirthDate.Ticks);
            Assert.AreEqual(contactNumber, target.ContactNumber);
        }

        [Test]
        [ExpectedException(typeof(UninitializableTargetTypeException))]
        public void Should_try_create_object_of_abstract_class_and_throw_exception()
        {
            long birthDateInTicks = DateTime.MinValue.Ticks;
            var contactNumber = "2939349575";

            // Arrange
            var source = new Person
            {
                BirthDate = new DateTime(birthDateInTicks),
                ContactNumber = contactNumber
            };

            // Act
            var mapper = new Mapper<Person, BasePerson>();
            var target = mapper.CreateAndMap(source);
        }

        [Test]
        public void Should_map_with_custom_mappings()
        {
            long birthDateInTicks = DateTime.MinValue.Ticks;
            var contactNumber = "2939349575";
            string name = "Czarek";

            // Arrange
            var source = new Person
            {
                Name = "Czarek",
                Sex = Sex.Male,
                BirthDate = new DateTime(birthDateInTicks),
                ContactNumber = contactNumber
            };

            var target = new Animal();

            // Act
            var mapper = new Mapper<Person, Animal>()
                .AddMap((p, a) => a.Gender = p.Sex)
                .AddMap((p, a) => a.Name = a.Name);
            mapper.Map(source, target);

            // Assert
            Assert.IsFalse(object.ReferenceEquals(source, target));
            Assert.AreEqual(name, target.Name);
            Assert.AreEqual(Sex.Male, target.Gender);
            Assert.IsNull(target.IsVaccinated);

        }

    }
}
