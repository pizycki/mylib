﻿using System;
using NUnit.Framework;

namespace IzyckiP.Core.Tests.IntegerTests
{
    [TestFixture]
    public class IntegerExtensionsTests
    {
        [Test]
        public void Should_accept_positive_number()
        {
            int number = 10;
            var isPositive = number.IsPositive();
            Assert.IsTrue(isPositive);
        }

        [Test]
        public void Should_accept_positive_nullable_number()
        {
            int? number = new Nullable<int>(10);
            var isPositive = number.IsPositive();
            Assert.IsTrue(isPositive);
        }

        [Test]
        public void Should_not_accept_null_number()
        {
            int? number = null;
            var isPositive = number.IsPositive();
            Assert.IsFalse(isPositive);
        }

        [Test]
        public void Should_not_accept_zero_as_positive()
        {
            int number = 0;
            var isPositive = number.IsPositive();
            Assert.IsFalse(isPositive);
        }
    }
}
