﻿using System;
using IzyckiP.Core.Tests.Dummies;
using NUnit.Framework;

namespace IzyckiP.Core.Tests.ReflectionUtils
{
    [TestFixture]
    public class ReflectionUtilsTests
    {
        [Test]
        public void Should_get_properties_name()
        {
            // Arrange
            var dummy = new Person();
            // Act
            var propertyName = Core.ReflectionUtils.GetPropertyName(() => dummy.ContactNumber);
            // Assert
            Assert.AreEqual("ContactNumber", propertyName);
        }

        [Test]
        public void Should_get_properties_name_for_DateTime()
        {
            // Arrange
            var dummy = new Person();
            // Act
            var propertyName = Core.ReflectionUtils.GetPropertyName(() => dummy.BirthDate);
            // Assert
            Assert.AreEqual("BirthDate", propertyName);
        }

        [Test]
        public void Should_get_all_members_and_property_name()
        {
            // Arrange
            var dummy = new Person();
            // Act
            var fullPropertyName = Core.ReflectionUtils.GetAllMembersAndPropertyName(() => dummy.ContactNumber);
            // Assert
            Assert.AreEqual("dummy.ContactNumber", fullPropertyName);
        }

        [Test]
        public void Should_get_value_of_property()
        {
            // Arrange
            const string number = "123456789";
            var dummy = new Person() { ContactNumber = number };
            var propertyName = Core.ReflectionUtils.GetPropertyName(() => dummy.ContactNumber);

            // Act
            var propertyValue = Core.ReflectionUtils.GetPropertyValue(dummy, propertyName);

            // Assert
            Assert.AreEqual(number, propertyValue);
        }

        [Test]
        public void Should_get_value_of_casted_property()
        {
            // Arrange
            DateTime actualTime = DateTime.Now;
            var dummy = new Person() { BirthDate = actualTime };
            var propertyName = Core.ReflectionUtils.GetPropertyName(() => dummy.BirthDate);

            // Act
            var propertyValue = Core.ReflectionUtils.GetPropertyValue<DateTime>(dummy, propertyName);

            // Assert
            Assert.AreEqual(actualTime, propertyValue);
        }
    }
}
