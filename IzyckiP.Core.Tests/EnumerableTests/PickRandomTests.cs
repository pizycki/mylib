﻿using System;
using System.Collections.Generic;
using System.Linq;
using IzyckiP.Core.Tests.Dummies;
using NUnit.Framework;

namespace IzyckiP.Core.Tests.EnumerableTests
{
    [TestFixture]
    public class PickRandomTests
    {
        IEnumerable<Animal> _animals;

        [SetUp]
        public void Setup()
        {
            _animals = new List<Animal>
            {
                new Animal{ Name="Rex", Gender = Sex.Male, IsVaccinated = true},
                new Animal{ Name="Alice", Gender = Sex.Female, IsVaccinated = true},
                new Animal{ Name="Czarek", Gender = Sex.Male, IsVaccinated = true},
                new Animal{ Name="Marry", Gender = Sex.Female, IsVaccinated = true},
                new Animal{ Name="Karo", Gender = Sex.Female, IsVaccinated = true},
            };
        }

        [Test]
        public void Should_pick_three_random_items()
        {
            // Arrange
            var items = _animals;

            // Act
            var shuffeled = items.PickRandom(3);

            // Assert
            Assert.AreEqual(3, shuffeled.Count());
        }


        [Test]
        public void Should_pick_not_same_random_items()
        {
            // Arrange
            var items = _animals;

            // Act
            var shuffeled = items.PickRandom(2);

            // Assert
            Assert.AreNotEqual(shuffeled.First().Name, shuffeled.Last().Name);
        }

        [Test]
        [ExpectedException(typeof(ArgumentException))]
        public void Should_pick_item_from_empty_list_and_throw_exception()
        {
            // Arrange
            var items = new List<Animal>();

            // Act
            var shuffeled = items.PickRandom();
        }

    }
}
