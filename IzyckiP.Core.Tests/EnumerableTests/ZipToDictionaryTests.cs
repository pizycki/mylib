﻿using System;
using System.Collections.Generic;
using NUnit.Framework;

namespace IzyckiP.Core.Tests.EnumerableTests
{
    [TestFixture]
    public class ZipToDictionaryTests
    {
        [Test]
        public void GetFirstValueOfZippedToDictionary_AndExpectCorrectValue()
        {
            // Arrange
            var ls1 = new List<int> { 0 };
            var ls2 = new List<string> { "TestoValue" };

            // Act
            var zippedDictionary = EnumerableExt.ZipToDictionary(ls1, ls2);

            // Assert
            Assert.AreEqual("TestoValue", zippedDictionary[0]);
        }

        [Test]
        public void GetSecondValueOfZippedToDictionary_AndExpectKeyNotFoundException()
        {
            // Arrange
            var ls1 = new List<int> { 0 };
            var ls2 = new List<string> { "TestoValue" };

            // Act
            var zippedDictionary = EnumerableExt.ZipToDictionary(ls1, ls2);

            // Assert
            try
            {
                var val = zippedDictionary[1];
            }
            catch (Exception ex)
            {
                Assert.IsInstanceOf<KeyNotFoundException>(ex);
            }
        }

        [Test]
        public void ZipToDictTwoNotEquallyLongLists_AndExpectCollectionsLengthNotEqualException_KeysLonger()
        {
            // Arrange
            var ls1 = new List<int> { 0, 1 };
            var ls2 = new List<string> { "TestoValue" };

            // Act and Assert exception
            try
            {
                var zippedDictionary = EnumerableExt.ZipToDictionary(ls1, ls2);
            }
            catch (Exception ex)
            {
                Assert.IsInstanceOf<CollectionsLengthNotEqualException>(ex);
            }
        }

        [Test]
        public void ZipToDictTwoNotEquallyLongLists_AndExpectCollectionsLengthNotEqualException_ValuesLonger()
        {
            // Arrange
            var ls1 = new List<int> { 0 };
            var ls2 = new List<string> { "TestoValue1", "TestoValue2" };

            // Act and Assert exception
            try
            {
                var zippedDictionary = EnumerableExt.ZipToDictionary(ls1, ls2);
            }
            catch (Exception ex)
            {
                Assert.IsInstanceOf<CollectionsLengthNotEqualException>(ex);
            }
        }
    }
}
