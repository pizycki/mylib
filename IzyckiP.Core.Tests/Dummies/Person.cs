﻿using System;

namespace IzyckiP.Core.Tests.Dummies
{
    public abstract class BasePerson
    {
    }

    public enum Sex { Male, Female }

    public class Person : BasePerson
    {
        public Sex Sex { get; set; }
        public string Name { get; set; }
        public string ContactNumber { get; set; }
        public DateTime BirthDate { get; set; }
    }

    public class Animal
    {
        public string Name { get; set; }
        public Sex Gender { get; set; }
        public bool? IsVaccinated { get; set; }
    }

    public class Employee : Person
    {
        public Person Mother { get; set; }
    }
}
