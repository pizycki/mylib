﻿using System;
using NUnit.Framework;

namespace IzyckiP.Core.Tests.DateTimeTests
{
    [TestFixture]
    public class DateTimeExtensionsTests
    {
        #region [ DateTimeHelper.CountYears ]
        [Test]
        public void Should_count_year_beetween_1970_and_2012()
        {
            // Arrange 
            var start = new DateTime(1970, 1, 1); // 1 Jan 1970
            var end = new DateTime(2012, 12, 31); // 31 Dec 2012

            // Act 
            var years = DateTimeHelper.CountYears(start, end);

            // Assert 
            Assert.AreEqual(42, years);
        }

        [Test]
        public void Should_count_year_beetween_1970_and_2013_and_fail()
        {
            // Arrange 
            var start = new DateTime(1970, 1, 1); // 1 Jan 1970
            var end = new DateTime(2013, 1, 1); // 1 Jan 2012

            // Act 
            var years = DateTimeHelper.CountYears(start, end);

            // Assert 
            Assert.AreNotEqual(42, years);
        }

        #endregion
    }
}
